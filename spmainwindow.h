#ifndef SPMAINWINDOW_H
#define SPMAINWINDOW_H

#include <KXmlGuiWindow>

class QLabel;
class QTableView;

class SpreadSheetMainWindow : public KXmlGuiWindow
{
    Q_OBJECT

public:
    explicit SpreadSheetMainWindow(QWidget *parent = Q_NULLPTR);

    static QString locationFromIndex(int row, int column);

private:
    void setupActions();
    void setupStatusBar();
    void setupSpreadSheet();

private slots:
    void newFile();
    void open();
    void save();
    void saveAs();
    void cut();
    void copy();
    void paste();

    void updateStatusBar();

private:
    QTableView *m_spreadsheet;
    QLabel *m_locationLabel;
    QLabel *m_formulaLabel;

    QVector< QVector<QVariant> > m_copyBuffer;
};

#endif
