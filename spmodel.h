#ifndef SPMODEL_H
#define SPMODEL_H

#include <QAbstractTableModel>

class SpreadSheetModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit SpreadSheetModel(QObject *parent = Q_NULLPTR);

    static QString rowNameFromIndex(int index);
    static QString columnNameFromIndex(int index);
    static int indexFromRowName(const QString &name);
    static int indexFromColumnName(const QString &name);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

private:
    QVariant evaluateFormula(const QString &formula) const;
    QVariant evaluateExpression(const QString &str, int &pos) const;
    QVariant evaluateTerm(const QString &str, int &pos) const;
    QVariant evaluateFactor(const QString &str, int &pos) const;

    QHash<QPair<int, int>, QVariant> m_data;
    mutable QVector< QPair<int, int> > m_visited;
};

#endif
