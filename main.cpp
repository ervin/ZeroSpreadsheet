#include <QApplication>

#include "spmainwindow.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    SpreadSheetMainWindow *window = new SpreadSheetMainWindow;
    window->show();

    return app.exec();
}

