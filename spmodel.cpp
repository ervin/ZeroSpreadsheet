#include "spmodel.h"

#include <QRegularExpression>

SpreadSheetModel::SpreadSheetModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QString SpreadSheetModel::rowNameFromIndex(int index)
{
    return QString::number(index + 1);
}

QString SpreadSheetModel::columnNameFromIndex(int index)
{
    QString name;

    while (index >= 0) {
        const QChar c = 'A' + index % 26;
        name = c + name;

        index /= 26;
        index--;
    }

    return name;
}

int SpreadSheetModel::indexFromRowName(const QString &name)
{
    return name.toLong() - 1;
}

int SpreadSheetModel::indexFromColumnName(const QString &name)
{
    int index = 0;

    for (int pos = 0; pos < name.size(); ++pos) {
        index *= 26;
        index += name[pos].toUpper().unicode() - 'A' + 1;
    }

    --index;

    return index;
}

int SpreadSheetModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    else
        return 1 << 16;
}

int SpreadSheetModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    else
        return 1 << 16;
}

QVariant SpreadSheetModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QAbstractTableModel::headerData(section, orientation, role);

    if (orientation == Qt::Vertical)
        return rowNameFromIndex(section);
    else
        return columnNameFromIndex(section);
}

QVariant SpreadSheetModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    const int row = index.row();
    const int col = index.column();
    const QPair<int, int> id(row, col);

    if (m_visited.contains(id)) {
        m_visited.clear();
        return "#Error";
    }

    m_visited << id;

    if (m_data.contains(id)) {
        const QVariant value = m_data[id];

        if (role == Qt::DisplayRole) {
            const QString str = value.toString();
            const QVariant result = str.startsWith('=') ? evaluateFormula(str.mid(1))
                                  : (str != " ") ? str.trimmed()
                                  : str;
            m_visited.clear();
            return result;
        } else {
            m_visited.clear();
            return value;
        }
    } else {
        m_visited.clear();
        return QVariant();
    }
}

bool SpreadSheetModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return QAbstractTableModel::setData(index, value, role);

    const int row = index.row();
    const int col = index.column();
    const QPair<int, int> id(row, col);

    if (!value.isValid())
        m_data.remove(id);
    else
        m_data[id] = value;

    emit dataChanged(index, index);
    return true;
}

QVariant SpreadSheetModel::evaluateFormula(const QString &formula) const
{
    int pos = 0;
    const QVariant result = evaluateExpression(formula, pos);
    return result.isValid() ? result : "#Error";
}

QVariant SpreadSheetModel::evaluateExpression(const QString &str, int &pos) const
{
    QVariant result = evaluateTerm(str, pos);

    while (pos < str.size()) {
        const QChar op = str[pos];

        if (op != '+' && op != '-')
            return result;

        ++pos;
        const QVariant term = evaluateTerm(str, pos);

        if (result.type() == QVariant::Double && term.type() == QVariant::Double) {
            if (op == '+')
                result = result.toDouble() + term.toDouble();
            else
                result = result.toDouble() - term.toDouble();
        } else {
            result = QVariant();
        }
    }

    return result;
}

QVariant SpreadSheetModel::evaluateTerm(const QString &str, int &pos) const
{
    QVariant result = evaluateFactor(str, pos);

    while (pos < str.size()) {
        const QChar op = str[pos];

        if (op != '*' && op != '/')
            return result;

        ++pos;
        const QVariant factor = evaluateFactor(str, pos);

        if (result.type() == QVariant::Double && factor.type() == QVariant::Double) {
            if (op == '*')
                result = result.toDouble() * factor.toDouble();
            else
                result = result.toDouble() / factor.toDouble();
        } else {
            result = QVariant();
        }
    }

    return result;
}

QVariant SpreadSheetModel::evaluateFactor(const QString &str, int &pos) const
{
    QVariant result;

    if (pos >= str.size()) {
        result = QVariant();
    } else if (str[pos] == '-') {
        ++pos;
        result = evaluateFactor(str, pos);

        if (result.type() == QVariant::Double)
            result = -result.toDouble();
        else
            result = QVariant();

    } else if (str[pos] == '(') {
        ++pos;
        result = evaluateExpression(str, pos);

        if (pos >= str.size() || str[pos] != ')') {
            result = QVariant();
        }

        ++pos;
    } else {
        QString token;

        while (pos < str.size() && (str[pos].isLetterOrNumber() || str[pos] == '.')) {
            token += str[pos];
            ++pos;
        }

        QRegularExpression regexp("([A-Za-z]+)([1-9][0-9]*)");
        QRegularExpressionMatch match = regexp.match(token);
        if (match.hasMatch()) {
            const int column = indexFromColumnName(match.captured(1));
            const int row = indexFromRowName(match.captured(2));
            const QVariant value = data(index(row, column));

            if (value.isValid()) {
                bool ok = false;
                result = value.toDouble(&ok);
                if (!ok) {
                    result = QVariant();
                }
            } else {
                result = 0.0;
            }
        } else {
            result = token.toDouble();
        }
    }

    return result;
}
