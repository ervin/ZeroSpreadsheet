#include <QtTest>

#include <QLabel>
#include <QTableView>

#include "spmainwindow.h"
#include "spmodel.h"

class SpreadSheetTest : public QObject
{
    Q_OBJECT
private:
    void verifySquareEmptiness(const SpreadSheetModel &m, int startRow, int startCol, int size)
    {
        for (int row = startRow; row < startRow+size; row++) {
            for (int col = startCol; col < startCol+size; col++) {
                QVariant data = m.data(m.index(row, col));
                QString fail_msg("Cell (%1, %2) is not empty...");
                QVERIFY2(data == QVariant(), fail_msg.arg(row).arg(col).toLatin1().data());
            }
        }
    }


private slots:
    void testLocationFromIndex_data()
    {
        QTest::addColumn<QString>("location");
        QTest::addColumn<int>("row");
        QTest::addColumn<int>("column");

        QTest::newRow("A1") << "A1" << 0 << 0;
        QTest::newRow("B1") << "B1" << 0 << 1;
        QTest::newRow("C2") << "C2" << 1 << 2;
        QTest::newRow("Y2") << "Y2" << 1 << 24;
        QTest::newRow("Z2") << "Z2" << 1 << 25;
        QTest::newRow("AA3") << "AA3" << 2 << 26;
        QTest::newRow("AB3") << "AB3" << 2 << 27;
        QTest::newRow("AZ25") << "AZ25" << 24 << 51;
        QTest::newRow("BA25") << "BA25" << 24 << 52;
        QTest::newRow("ZZ25") << "ZZ25" << 24 << 701;
        QTest::newRow("AAA25") << "AAA25" << 24 << 702;
    }

    void testLocationFromIndex()
    {
        QFETCH(QString, location);
        QFETCH(int, row);
        QFETCH(int, column);

        QString res = SpreadSheetMainWindow::locationFromIndex(row, column);
        QCOMPARE(res, location);
    }

    void testModelCreation()
    {
        SpreadSheetModel m;

        QCOMPARE(m.columnCount(), 65536);
        QCOMPARE(m.rowCount(), 65536);
        QCOMPARE(m.columnCount(m.index(10, 50)), 0);
        QCOMPARE(m.rowCount(m.index(30, 600)), 0);

        verifySquareEmptiness(m, 0, 0, 20);
        verifySquareEmptiness(m, 65500, 65500, 36);
        verifySquareEmptiness(m, 31000, 4000, 100);
    }

    void testNamesFromIndex_data()
    {
        QTest::addColumn<int>("index");
        QTest::addColumn<QString>("rowName");
        QTest::addColumn<QString>("columnName");

        QTest::newRow("0") << 0 << "1" << "A";
        QTest::newRow("1") << 1 << "2" << "B";
        QTest::newRow("2") << 2 << "3" << "C";
        QTest::newRow("24") << 24 << "25" << "Y";
        QTest::newRow("25") << 25 << "26" << "Z";
        QTest::newRow("26") << 26 << "27" << "AA";
        QTest::newRow("27") << 27 << "28" << "AB";
        QTest::newRow("51") << 51 << "52" << "AZ";
        QTest::newRow("52") << 52 << "53" << "BA";
        QTest::newRow("701") << 701 << "702" << "ZZ";
        QTest::newRow("702") << 702 << "703" << "AAA";
    }

    void testNamesFromIndex()
    {
        QFETCH(int, index);
        QFETCH(QString, rowName);
        QFETCH(QString, columnName);

        QCOMPARE(SpreadSheetModel::rowNameFromIndex(index), rowName);
        QCOMPARE(SpreadSheetModel::columnNameFromIndex(index), columnName);
    }

    void testHeaderData_data()
    {
        QTest::addColumn<int>("section");
        QTest::addColumn<int>("orientation");
        QTest::addColumn<QString>("headerData");

        QTest::newRow("0V") << 0 << (int)Qt::Vertical << "1";
        QTest::newRow("1V") << 1 << (int)Qt::Vertical << "2";
        QTest::newRow("2V") << 2 << (int)Qt::Vertical << "3";
        QTest::newRow("24V") << 24 << (int)Qt::Vertical << "25";
        QTest::newRow("25V") << 25 << (int)Qt::Vertical << "26";
        QTest::newRow("26V") << 26 << (int)Qt::Vertical << "27";
        QTest::newRow("27V") << 27 << (int)Qt::Vertical << "28";
        QTest::newRow("51V") << 51 << (int)Qt::Vertical << "52";
        QTest::newRow("52V") << 52 << (int)Qt::Vertical << "53";
        QTest::newRow("701V") << 701 << (int)Qt::Vertical << "702";
        QTest::newRow("702V") << 702 << (int)Qt::Vertical << "703";

        QTest::newRow("0H") << 0 << (int)Qt::Horizontal << "A";
        QTest::newRow("2H") << 2 << (int)Qt::Horizontal << "C";
        QTest::newRow("24H") << 24 << (int)Qt::Horizontal << "Y";
        QTest::newRow("25H") << 25 << (int)Qt::Horizontal << "Z";
        QTest::newRow("26H") << 26 << (int)Qt::Horizontal << "AA";
        QTest::newRow("27H") << 27 << (int)Qt::Horizontal << "AB";
        QTest::newRow("51H") << 51 << (int)Qt::Horizontal << "AZ";
        QTest::newRow("52H") << 52 << (int)Qt::Horizontal << "BA";
        QTest::newRow("701H") << 701 << (int)Qt::Horizontal << "ZZ";
        QTest::newRow("702H") << 702 << (int)Qt::Horizontal << "AAA";
    }

    void testHeaderData()
    {
        QFETCH(int, section);
        QFETCH(int, orientation);
        QFETCH(QString, headerData);

        SpreadSheetModel m;

        QCOMPARE(m.headerData(section, (Qt::Orientation)orientation).toString(), headerData);
    }

    void testThatTextIsStored()
    {
        SpreadSheetModel m;
        QModelIndex index = m.index(21, 0);

        m.setData(index, "A string");
        QCOMPARE(m.data(index).toString(), QString("A string"));

        m.setData(index, "A different string");
        QCOMPARE(m.data(index).toString(), QString("A different string"));

        m.setData(index, "");
        QCOMPARE(m.data(index).toString(), QString(""));
    }

    void testThatManyCellsExist()
    {
        SpreadSheetModel m;
        QModelIndex a = m.index(0, 0);
        QModelIndex b = m.index(23, 26);
        QModelIndex c = m.index(699, 900);

        m.setData(a, "One");
        m.setData(b, "Two");
        m.setData(c, "Three");

        QCOMPARE(m.data(a).toString(), QString("One"));
        QCOMPARE(m.data(b).toString(), QString("Two"));
        QCOMPARE(m.data(c).toString(), QString("Three"));

        m.setData(a, "Four");

        QCOMPARE(m.data(a).toString(), QString("Four"));
        QCOMPARE(m.data(b).toString(), QString("Two"));
        QCOMPARE(m.data(c).toString(), QString("Three"));
    }

    void testNumericCells()
    {
        SpreadSheetModel m;
        QModelIndex index = m.index(0, 20);

        m.setData(index, "X99"); // String
        QCOMPARE(m.data(index).toString(), QString("X99"));

        m.setData(index, "14"); // Number
        QCOMPARE(m.data(index).toString(), QString("14"));
        QCOMPARE(m.data(index).toInt(), 14);

        m.setData(index, "99 X"); // Whole string must be numeric
        QCOMPARE(m.data(index).toString(), QString("99 X"));
        bool ok;
        m.data(index).toInt(&ok);
        QVERIFY(!ok);

        m.setData(index, " 1234 "); // Blanks ignored
        QCOMPARE(m.data(index).toString(), QString("1234"));
        QCOMPARE(m.data(index).toInt(), 1234);

        m.setData(index, " "); // Just a blank
        QCOMPARE(m.data(index).toString(), QString(" "));
    }

    void testAccessToLiteralForEditing()
    {
        SpreadSheetModel m;
        QModelIndex index = m.index(0, 20);

        m.setData(index, "Some string");
        QCOMPARE(m.data(index, Qt::EditRole).toString(), QString("Some string"));

        m.setData(index, " 1234 ");
        QCOMPARE(m.data(index, Qt::EditRole).toString(), QString(" 1234 "));

        m.setData(index, "=7");
        QCOMPARE(m.data(index, Qt::EditRole).toString(), QString("=7"));
    }

    void testFormulaBasics()
    {
        SpreadSheetModel m;
        QModelIndex index = m.index(2, 10);

        m.setData(index, " =7"); // note leading space, not a formula
        QCOMPARE(m.data(index).toString(), QString("=7"));
        QCOMPARE(m.data(index, Qt::EditRole).toString(), QString(" =7"));

        m.setData(index, "=7"); // constant formula
        QCOMPARE(m.data(index).toString(), QString("7"));
        QCOMPARE(m.data(index, Qt::EditRole).toString(), QString("=7"));

        // Adding intermediate tests here to guide you is fine
        // Go ahead!

        m.setData(index, "=(7)"); // parenthesis
        QCOMPARE(m.data(index).toString(), QString("7"));

        m.setData(index, "=(((10)))"); // more parenthesis
        QCOMPARE(m.data(index).toString(), QString("10"));

        m.setData(index, "=2*3*4"); // multiply
        QCOMPARE(m.data(index).toString(), QString("24"));

        m.setData(index, "=2/4*3"); // divide
        QCOMPARE(m.data(index).toString(), QString("1.5"));

        m.setData(index, "=12+3+4"); // add
        QCOMPARE(m.data(index).toString(), QString("19"));

        m.setData(index, "=12-3+4"); // substract
        QCOMPARE(m.data(index).toString(), QString("13"));

        m.setData(index, "=4+3*2"); // precedence
        QCOMPARE(m.data(index).toString(), QString("10"));

        m.setData(index, "=4+3*-2"); // precedence and negate
        QCOMPARE(m.data(index).toString(), QString("-2"));

        m.setData(index, "=5*(4+3)*(((2+1)))"); // full expression
        QCOMPARE(m.data(index).toString(), QString("105"));
    }

    void testFormulaErrors()
    {
        SpreadSheetModel m;
        QModelIndex index = m.index(0, 0);

        m.setData(index, "=5*");
        QCOMPARE(m.data(index).toString(), QString("#Error"));

        m.setData(index, "=((((5))");
        QCOMPARE(m.data(index).toString(), QString("#Error"));
    }

    void testIndexFromNames_data()
    {
        QTest::addColumn<int>("index");
        QTest::addColumn<QString>("rowName");
        QTest::addColumn<QString>("columnName");

        QTest::newRow("0") << 0 << "1" << "A";
        QTest::newRow("1") << 1 << "2" << "B";
        QTest::newRow("2") << 2 << "3" << "C";
        QTest::newRow("24") << 24 << "25" << "Y";
        QTest::newRow("25") << 25 << "26" << "Z";
        QTest::newRow("26") << 26 << "27" << "AA";
        QTest::newRow("27") << 27 << "28" << "AB";
        QTest::newRow("51") << 51 << "52" << "AZ";
        QTest::newRow("52") << 52 << "53" << "BA";
        QTest::newRow("701") << 701 << "702" << "ZZ";
        QTest::newRow("702") << 702 << "703" << "AAA";
    }

    void testIndexFromNames()
    {
        QFETCH(int, index);
        QFETCH(QString, rowName);
        QFETCH(QString, columnName);

        QCOMPARE(SpreadSheetModel::indexFromRowName(rowName), index);
        QCOMPARE(SpreadSheetModel::indexFromColumnName(columnName), index);
    }

    void testThatReferenceWorks()
    {
        SpreadSheetModel m;
        QModelIndex a = m.index(0, 0);
        QModelIndex b = m.index(10, 20);

        m.setData(a, "8");
        m.setData(b, "=A1");

        QCOMPARE(m.data(b).toString(), QString("8"));
    }

    void testThatChangesPropagate()
    {
        SpreadSheetModel m;
        QModelIndex a = m.index(0, 0);
        QModelIndex b = m.index(10, 20);

        m.setData(a, "8");
        m.setData(b, "=A1");

        QCOMPARE(m.data(b).toString(), QString("8"));

        m.setData(a, "9");
        QCOMPARE(m.data(b).toString(), QString("9"));
    }

    void testThatFormulasRecalculate()
    {
        SpreadSheetModel m;
        QModelIndex a = m.index(0, 0);
        QModelIndex b = m.index(1, 0);
        QModelIndex c = m.index(0, 1);

        m.setData(a, "8");
        m.setData(b, "3");
        m.setData(c, "=A1*(A1-A2)+A2/3");

        QCOMPARE(m.data(c).toString(), QString("41"));

        m.setData(b, "6");

        QCOMPARE(m.data(c).toString(), QString("18"));
    }

    void testThatDeepChangesPropagate()
    {
        SpreadSheetModel m;
        QModelIndex a1 = m.index(0, 0);
        QModelIndex a2 = m.index(1, 0);
        QModelIndex a3 = m.index(2, 0);
        QModelIndex a4 = m.index(3, 0);

        m.setData(a1, "8");
        m.setData(a2, "=A1");
        m.setData(a3, "=A2");
        m.setData(a4, "=A3");

        QCOMPARE(m.data(a4).toString(), QString("8"));

        m.setData(a2, "6");

        QCOMPARE(m.data(a4).toString(), QString("6"));
    }

    void testThatFormulasWorkWithManyCells()
    {
        SpreadSheetModel m;
        QModelIndex a1 = m.index(0, 0);
        QModelIndex a2 = m.index(1, 0);
        QModelIndex a3 = m.index(2, 0);
        QModelIndex a4 = m.index(3, 0);
        QModelIndex b1 = m.index(0, 1);
        QModelIndex b2 = m.index(1, 1);
        QModelIndex b3 = m.index(2, 1);
        QModelIndex b4 = m.index(3, 1);

        m.setData(a1, "10");
        m.setData(a2, "=A1+B1");
        m.setData(a3, "=A2+B2");
        m.setData(a4, "=A3");
        m.setData(b1, "7");
        m.setData(b2, "=A2");
        m.setData(b3, "=A3-A2");
        m.setData(b4, "=A4+B3");

        QCOMPARE(m.data(a4).toString(), QString("34"));
        QCOMPARE(m.data(b4).toString(), QString("51"));
    }

    void testCircularReferences()
    {
        SpreadSheetModel m;
        QModelIndex a1 = m.index(0, 0);
        QModelIndex a2 = m.index(1, 0);
        QModelIndex a3 = m.index(2, 0);

        m.setData(a1, "=A1");
        m.setData(a2, "=A1");
        m.setData(a3, "=A2");

        QCOMPARE(m.data(a1).toString(), QString("#Error"));

        m.setData(a1, "=A3");

        QCOMPARE(m.data(a1).toString(), QString("#Error"));
    }

    void testFormulaInStatusBar()
    {
        SpreadSheetMainWindow w;

        QTableView * const table = w.findChild<QTableView*>("spreadsheet_view");
        QVERIFY(table!=0);

        const QLabel * const label = w.findChild<QLabel*>("formula_label");
        QVERIFY(table!=0);

        QAbstractItemModel * const model = table->model();
        QVERIFY(model!=0);

        const QModelIndex a1 = model->index(0, 0);
        const QModelIndex a2 = model->index(1, 0);

        model->setData(a1, "10");
        model->setData(a2, "=A1*2");

        table->setCurrentIndex(a1);
        QCOMPARE(label->text(), QString("10"));
        table->setCurrentIndex(a2);
        QCOMPARE(label->text(), QString("=A1*2"));
    }
};

QTEST_MAIN(SpreadSheetTest)

#include "sptest.moc"
