#include "spmainwindow.h"

#include <KActionCollection>

#include <QDebug>
#include <QItemSelectionModel>
#include <QLabel>
#include <QStatusBar>
#include <QTableView>

#include "spmodel.h"

SpreadSheetMainWindow::SpreadSheetMainWindow(QWidget *parent)
    : KXmlGuiWindow(parent),
      m_spreadsheet(Q_NULLPTR),
      m_locationLabel(Q_NULLPTR),
      m_formulaLabel(Q_NULLPTR)
{
    setupActions();
    setupStatusBar();
    setupSpreadSheet();
}

QString SpreadSheetMainWindow::locationFromIndex(int row, int column)
{
    return SpreadSheetModel::columnNameFromIndex(column)
         + SpreadSheetModel::rowNameFromIndex(row);
}

void SpreadSheetMainWindow::setupActions()
{
    KActionCollection * const ac = actionCollection();

    // File actions
    ac->addAction(KStandardAction::New, this, SLOT(newFile()));
    ac->addAction(KStandardAction::Open, this, SLOT(open()));
    ac->addAction(KStandardAction::Save, this, SLOT(save()));
    ac->addAction(KStandardAction::SaveAs, this, SLOT(saveAs()));
    ac->addAction(KStandardAction::Quit, this, SLOT(close()));

    // Edit actions
    ac->addAction(KStandardAction::Cut, this, SLOT(cut()));
    ac->addAction(KStandardAction::Copy, this, SLOT(copy()));
    ac->addAction(KStandardAction::Paste, this, SLOT(paste()));

    setupGUI();
}

void SpreadSheetMainWindow::setupStatusBar()
{
    m_locationLabel = new QLabel(" WWW999 ", this);
    m_locationLabel->setAlignment(Qt::AlignHCenter);
    m_locationLabel->setMinimumSize(m_locationLabel->sizeHint());
    statusBar()->addWidget(m_locationLabel);

    m_formulaLabel = new QLabel(this);
    m_formulaLabel->setObjectName("formula_label");
    m_formulaLabel->setIndent(3);
    statusBar()->addWidget(m_formulaLabel, 1);
}

void SpreadSheetMainWindow::setupSpreadSheet()
{
    m_spreadsheet = new QTableView(this);
    m_spreadsheet->setObjectName("spreadsheet_view");

    QAbstractItemModel *model = new SpreadSheetModel(this);
    m_spreadsheet->setModel(model);

    m_spreadsheet->addAction(actionCollection()->action("edit_cut"));
    m_spreadsheet->addAction(actionCollection()->action("edit_copy"));
    m_spreadsheet->addAction(actionCollection()->action("edit_paste"));
    m_spreadsheet->setContextMenuPolicy(Qt::ActionsContextMenu);

    connect(m_spreadsheet->selectionModel(), &QItemSelectionModel::currentChanged,
            this, &SpreadSheetMainWindow::updateStatusBar);

#if 0
    model->setData(model->index(0, 0), "William");
    model->setData(model->index(0, 1), "Adama");
    model->setData(model->index(0, 2), "1947-02-24");

    model->setData(model->index(1, 0), "Saul");
    model->setData(model->index(1, 1), "Tigh");
    model->setData(model->index(1, 2), "1949-03-22");

    model->setData(model->index(2, 0), "Lee");
    model->setData(model->index(2, 1), "Adama");
    model->setData(model->index(2, 2), "1973-04-03");

    model->setData(model->index(3, 0), "Kara");
    model->setData(model->index(3, 1), "Thrace");
    model->setData(model->index(3, 2), "1980-04-08");

    model->setData(model->index(4, 0), "Laura");
    model->setData(model->index(4, 1), "Roslin");
    model->setData(model->index(4, 2), "1952-04-28");

    model->setData(model->index(5, 0), "Gaius");
    model->setData(model->index(5, 1), "Baltar");
    model->setData(model->index(5, 2), "1971-06-04");
#endif

    setCentralWidget(m_spreadsheet);
}

void SpreadSheetMainWindow::newFile()
{
    qDebug() << Q_FUNC_INFO;
}

void SpreadSheetMainWindow::open()
{
    qDebug() << Q_FUNC_INFO;
}

void SpreadSheetMainWindow::save()
{
    qDebug() << Q_FUNC_INFO;
}

void SpreadSheetMainWindow::saveAs()
{
    qDebug() << Q_FUNC_INFO;
}

void SpreadSheetMainWindow::cut()
{
    copy();

    QAbstractItemModel * const model = m_spreadsheet->model();
    const QItemSelectionRange range = m_spreadsheet->selectionModel()->selection()[0];

    for (int col = range.left(); col <= range.right(); col++) {
        for (int row = range.top(); row <= range.bottom(); row++) {
            model->setData(model->index(row, col), QVariant());
        }
    }
}

void SpreadSheetMainWindow::copy()
{
    m_copyBuffer.clear();

    const QAbstractItemModel * const model = m_spreadsheet->model();
    const QItemSelectionRange range = m_spreadsheet->selectionModel()->selection()[0];

    for (int col = range.left(); col <= range.right(); col++) {
        m_copyBuffer << QVector<QVariant>();

        for (int row = range.top(); row <= range.bottom(); row++) {
            m_copyBuffer.last() << model->data(model->index(row, col), Qt::EditRole);
        }
    }
}

void SpreadSheetMainWindow::paste()
{
    int row = m_spreadsheet->currentIndex().row();
    int col = m_spreadsheet->currentIndex().column();

    QAbstractItemModel * const model = m_spreadsheet->model();

    foreach (const QVector<QVariant> &column, m_copyBuffer) {
        foreach(const QVariant &data, column) {
            model->setData(model->index(row, col), data);
            row++;
        }
        col++;
        row = m_spreadsheet->currentIndex().row();
    }
}

void SpreadSheetMainWindow::updateStatusBar()
{
    const QModelIndex index = m_spreadsheet->selectionModel()->currentIndex();

    const QVariant data = m_spreadsheet->model()->data(index, Qt::EditRole);
    const QString location = locationFromIndex(index.row(), index.column());

    m_locationLabel->setText(location);
    m_formulaLabel->setText(data.toString());
}
